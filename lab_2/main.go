package main

import (
  "fmt"
  "./algorithms"
)

// @see https://en.wikipedia.org/wiki/Matrix_(mathematics)#Linear_equations

// В матричной форме система линейных алгебраических уравнений
// представляется как AX=B

// Factors (an m-by-n matrix)
var A = [4][4]float64{
  {3, 1, -1, 2},
  {-5, 1, 3, -4},
  {2, 0, 1, -1},
  {1, -5, 3, -3},
}
// Solution (a column vector)
var X = [4]float64{
  1,
  -1,
  2,
  3,
}
// Free members (an m×1-column vector)
var B = [4]float64{
  6,
  -12,
  1,
  3,
}

// Чтобы такая система уравнений имела единственное решение, входящие
// в нее n уравнений должны быть линейно независимыми. Необходимым и
// достаточным условием этого является неравенство нулю определителя
// данной системы

// Алгоритмы решения систем уравнений такого типа делятся
// на прямые и итерационные

func main()  {
  var mx [4]float64
  var err error

  fmt.Println()
  fmt.Println("AX=B")
  fmt.Println("A", A)
  fmt.Println("B", B)
  fmt.Println("X", X)
  fmt.Println()

  // ----------------

  fmt.Println("Gauss method:")

  mx, err = algorithms.Gauss(A, B)

  if err == nil {
    fmt.Println("mx", mx)
  } else {
    fmt.Println(err)
  }

  fmt.Println()
}

