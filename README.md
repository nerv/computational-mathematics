# Computational Mathematics


## Solving equations
+ [Linear equations](lab_2)
  + [Gauss method](lab_2/algorithms/gauss.go)
+ [NonLinear equations](lab_3)
  + [Half-division method](lab_3/algorithms/half-division.go)
  + [Newton method](lab_3/algorithms/newton.go)
  + [Chords method](lab_3/algorithms/chords.go)


## Useful links
- [Введение в программирование на Go](http://golang-book.ru/)
- [Производная квадратного корня](http://ru.solverbook.com/spravochnik/proizvodnye/proizvodnaya-kornya-iks/)
- [Производная функции](https://www.kontrolnaya-rabota.ru/s/proizvodnaya-funktsii/one/)


![Golang logo](golang_logo.png)