package algorithms

import (
  "errors"
  "math"
)

type F func(float64) float64

/*
  Для этого метода существенно, чтобы функция f(x) была непрерывна
  и ограничена в заданном интервале [a, b], внутри которого находится
  корень. Предполагается также, что значения функции на концах интервала
  f(a) и f(b) имеют разные знаки, т.е. выполняется условие f(a)f(b) < 0.
 */
func HalfDivision(epsilon float64, interval [2]float64, f F) (float64, error) {
  // Обозначим исходный интервал [a, b] как [a0, b0]
  var a float64 = interval[0]
  var b float64 = interval[1]

  if f(a) * f(b) >= 0 {
    return math.NaN(), errors.New("A function should have different signs at the ends of an interval")
  }

  var x float64

  // Если требуется определить корень x* с погрешностью ε,
  // то деление исходного интервала [a, b] продолжают до тех пор,
  // пока длина отрезка [ai, bi] не станет меньше 2ε
  for math.Abs(a - b) > epsilon  {
    // Для нахождения корня уравнения f(x) = 0 отрезок [a, b] делится пополам,
    // т.е. вычисляется начальное приближение x0 = (a0 + b0)/2.
    x = (a + b) / 2

    // Если f(x0) = 0
    if f(x) == 0 {
      // значение x0 = x* является корнем уравнения
      return x, nil
    } else {
      // В противном случае выбирается один из отрезков [a0, x0] или [x0, b0],
      // на концах которого функция f(x) имеет разные знаки,
      // так как корень лежит в этой половине.
      if f(a) * f(x) < 0 {
        b = x
      } else if f(b) * f(x) < 0 {
        a = x
      } else {
        return math.NaN(), errors.New("A function should have different signs at least at one of two ranges")
      }
    }
  }

  return x, nil
}

