package main

import (
  "fmt"
  "math"
  "./algorithms"
)


// Task's data
var epsilon float64 = 0.0001
var interval [2]float64 = [2]float64{2, 3}

// f(x) = x - sqrt(9+x) + x^2 - 4
func f(x float64) float64 {
  return x - math.Pow(9 + x, 0.5) + math.Pow(x, 2) - 4
}

// f(x)' = 1 - 0.5 * pow(9 + x, -0.5) + 2x
func f1(x float64) float64 {
  return 1 - 0.5 * math.Pow(9 + x, -0.5) + 2 * x
}

// f(x)'' = 0.25 * pow(9 + x, -1.5) + 2
func f2(x float64) float64 {
  return 0.25 * math.Pow(9 + x, -1.5) + 2
}


// Implementation demo
func main() {
  var fx float64
  var err error

  fmt.Println()
  fmt.Println("epsilon", epsilon)
  fmt.Println("interval", interval)
  fmt.Println()

  // ----------------

  fmt.Println("Half-division method:")

  fx, err = algorithms.HalfDivision(epsilon, interval, f)

  if err == nil {
    fmt.Println("f(x)", fx)
  } else {
    fmt.Println(err)
  }

  fmt.Println()

  // ----------------

  fmt.Println("Newton method:")

  fx, err = algorithms.Newton(epsilon, interval, f, f1, f2)

  if err == nil {
    fmt.Println("f(x)", fx)
  } else {
    fmt.Println(err)
  }

  fmt.Println()

  // ----------------

  fmt.Println("Chords method:")

  fx, err = algorithms.Chords(epsilon, interval, f, f2)

  if err == nil {
    fmt.Println("f(x)", fx)
  } else {
    fmt.Println(err)
  }

  fmt.Println()
}

